package com.example.michael.tictac;

import android.content.DialogInterface;
import android.graphics.Point;
import android.os.PersistableBundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import static android.R.attr.onClick;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button[][] buttons = new Button[3][3];
//    private boolean player1turn = true;
    private int roundCount = 0;
    private int player1Points;
    private int player2Points;


    private boolean player = true;
    private boolean computer;

    private TextView textViewPlayer1;
    private TextView textViewPlayer2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewPlayer1 = findViewById(R.id.text_view_p1);
        textViewPlayer2 = findViewById(R.id.text_view_p2);

        for( int i = 0; i < 3; i++) {
            for(int j =0; j<3 ; j++) {
                String buttonID = "button_" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i][j] = findViewById(resID);
                buttons[i][j].setOnClickListener(this);

            }
        }

        Button buttonReset = findViewById(R.id.button_reset);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetGame();
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(!((Button) view).getText().toString().equals("")) {
            return;
        }

        if( player ) {
            ((Button) view).setText("X");
            player = false;
            computer = true;
            roundCount++;
            if( check() ) {
                return;
            }
        }

        if( computer ) {
            if(roundCount < 9) {
                while(true) {
                    int x = new Random().nextInt(3);
                    int y = new Random().nextInt(3);
                    Button selected = buttons[x][y];
                    if (selected.getText() == "") {
                        buttons[x][y].setText("O");
                        computer = false;
                        player = true;
                        roundCount++;
                        break;
                    }
                }
            }

        }
        check();
    }

    private boolean check() {
        if(checkForWin()) {
            if(!player) {
                Log.d("Maykel", "PLayer : ");
                player1Wins();
                return true;
            } else if(!computer) {
                player2Wins();
                return false;
            }
        } else if( roundCount == 9) {
            draw();
            return false;
        }
        return false;
    }

    private void selectButton() {
        int x = new Random().nextInt(3);
        int y = new Random().nextInt(3);
        Button selected = buttons[x][y];
        buttons[x][y].performClick();
    }

    private boolean checkForWin() {
        String[][] field = new String[3][3];
        for( int i =0;i<3;i++) {
            for(int j=0;j<3;j++) {
                field[i][j] = buttons[i][j].getText().toString();
            }
        }

        for(int i = 0; i<3; i++) {
            if(field[i][0].equals(field[i][1])
                    && field[i][0].equals(field[i][2])
                    && !field[i][0].equals("")) {
                return true;
            }
        }

        for(int i = 0; i<3; i++) {
            if(field[0][i].equals(field[1][i])
                    && field[0][i].equals(field[2][i])
                    && !field[0][i].equals("")) {
                return true;
            }
        }

        if(field[0][0].equals(field[1][1])
                && field[0][0].equals(field[2][2])
                && !field[0][0].equals("")) {
            return true;
        }

        if(field[0][2].equals(field[1][1])
                && field[0][2].equals(field[2][0])
                && !field[0][2].equals("")) {
            return true;
        }

        return false;
    }
    private void player1Wins() {
        player1Points ++;
        updatePointsText();
        showPlayerWinMessage();

    }
    private void player2Wins() {
        player2Points ++;
        updatePointsText();
        showComputerWinMessage();
    }
    private void draw() {
        Toast.makeText(this,"Remis!", Toast.LENGTH_SHORT).show();
        resetBoard();
    }

    private void updatePointsText() {
        textViewPlayer1.setText("Gracz : " + player1Points);
        textViewPlayer2.setText("Komputer : " + player2Points);
    }

    private void resetBoard() {
        for( int i =0;i<3;i++) {
            for(int j=0;j<3;j++) {
                buttons[i][j].setText("");
            }
        }
        roundCount = 0;
//        player1turn = true;
    }

    private void resetGame() {
        player1Points = 0;
        player2Points = 0;
        updatePointsText();
        resetBoard();
    }

    private void showPlayerWinMessage() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Brawo!");
        alertDialog.setMessage("Wygrałeś tę rundę");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        resetBoard();
                        selectButton();
                    }
                });
        alertDialog.show();
    }

    private void showComputerWinMessage() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Szkoda!");
        alertDialog.setMessage("Przegrałeś");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        resetBoard();
                    }
                });
        alertDialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("player1Points", player1Points);
        outState.putInt("player2Points", player2Points);
        outState.putInt("roundCount", roundCount);
        outState.putBoolean("computer", computer);
        outState.putBoolean("player", player);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        player1Points = savedInstanceState.getInt("player1Points");
        player2Points = savedInstanceState.getInt("player2Points");
        roundCount = savedInstanceState.getInt("roundCount");
        computer = savedInstanceState.getBoolean("computer");
        player = savedInstanceState.getBoolean("player");
    }
}
